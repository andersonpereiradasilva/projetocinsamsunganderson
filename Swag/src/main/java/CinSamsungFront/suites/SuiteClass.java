package CinSamsungFront.suites;
import static CinSamsungFront.common.DriverFactory.getDriver;
import static CinSamsungFront.common.DriverFactory.killDriver;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import CinSamsungFront.common.DriverFactory;
import CinSamsungFront.pages.LoginPage;
import CinSamsungFront.tests.TestLoginPage;
import CinSamsungFront.tests.TestMenuPage;
import CinSamsungFront.tests.TestProducts;



@RunWith(Suite.class)
@SuiteClasses({
	TestLoginPage.class,
	TestMenuPage.class,
	TestProducts.class,
})
public class SuiteClass {

	private static LoginPage pageLogin=new LoginPage();
	
	@BeforeClass
	public static void initializes() {
		getDriver().get("https://www.saucedemo.com/");
		DriverFactory.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals("Swag Labs", pageLogin.getTitle());

	}
	
	@AfterClass
	public static void ends() {
		killDriver(); 
	}
}
