package CinSamsungFront.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import CinSamsungFront.common.BasePage;

public class ProductsPage extends BasePage {

	
	public void sortAscendingName() {
		option(By.xpath("//*[@class='product_sort_container']"), "Name (A to Z)");
	}
	public void sortDescendingName() {
		option(By.xpath("//*[@class='product_sort_container']"), "Name (Z to A)");
	}

	public void PriceLowToHigh() {
		option(By.xpath("//*[@class='product_sort_container']"), "Price (low to high)");
	}
	
	public void PriceHighToLow() {
		option(By.xpath("//*[@class='product_sort_container']"), "Price (high to low)");
	}
	
	public String getProduct() {
		return text(By.xpath("//div[@class='inventory_item'][1]//div[@class='inventory_item_name']"));
	}
	
	public void goCart() {
		button(By.id("shopping_cart_container"));
	}
	
	public void addProduct(String id) {
		button(By.xpath("//div[@class='inventory_item']["+id+"]//button[@class='btn_primary btn_inventory']"));
		
	}
	
	public String getLabelButton(String id) {
		return geButtontLabel(By.xpath("//div[@class='inventory_item']["+id+"]//button[.='REMOVE']"));
	}

	public String getCartItems() {
		return value(By.xpath("//span[@class='fa-layers-counter shopping_cart_badge']"));
	}
	
	public String getHeaderTitle() {
		return header(By.xpath("//div[.='Products']"));
	}
	
	public void AddToCart(String id) {
		button(By.xpath("//div[@class='inventory_list']//div["+id+"]//div[3]//button[1]"));
	}
	public String getTextAddToCart(String id) {
		return text(By.xpath("//div[@class='inventory_list']//div["+id+"]//div[3]//button[1]"));
	}
}

