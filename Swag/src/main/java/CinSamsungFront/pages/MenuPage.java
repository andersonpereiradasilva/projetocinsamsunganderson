package CinSamsungFront.pages;

import org.openqa.selenium.By;

import CinSamsungFront.common.BasePage;

public class MenuPage extends BasePage {
	
	public void openMenu() {
		button(By.xpath("//button[.='Open Menu']"));
	}
	
	public void logout() {
		option(By.id("logout_sidebar_link"));
	}
	
	public void closeMenu() {
		button(By.xpath("//button[contains(text(),'Close Menu')]"));
	}
	
	public String getMenu() {
		return text(By.xpath("//nav[@class='bm-item-list']"));
	}
	
	public String getMenuLogout() {
		return text(By.xpath("//nav[@class='bm-item-list']//a[contains(text(),'Logout')]"));
	}
	
	public void MenuResetAppState() {
		//return text(By.xpath("//span[@class='fa-layers-counter shopping_cart_badge']"));
		//return text(By.xpath("//a[@id='reset_sidebar_link']"));
		button(By.xpath("//a[@id='reset_sidebar_link']"));
	}

}
