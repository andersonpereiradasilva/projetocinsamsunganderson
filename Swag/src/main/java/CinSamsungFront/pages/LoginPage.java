package CinSamsungFront.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import CinSamsungFront.common.DriverFactory;

import CinSamsungFront.common.BasePage;

public class LoginPage extends BasePage {
	// Returns the title of the page accessed
	public String getTitle() {
		return title();
	}
	
	public void setUserName(String value) {
		write(By.id("user-name"), value);
	}
	
	public void setPassword(String value) {
		write(By.id("password"), value);
	}
	
	public void login() {
		button(By.xpath("//input[@class='btn_action']"));
	}
	
	public String errorMessage() {
		return message(By.tagName("h3"));
	}
	
	/*
	 * public void openMenu() {
	 * button(By.xpath("//button[contains(text(),'Open Menu')]")); }
	 *  
	 * public void menuLogout() { 
	 * button(By.xpath("//a[@id='logout_sidebar_link']")); }
	 */
	

}
