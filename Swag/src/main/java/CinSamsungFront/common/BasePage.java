package CinSamsungFront.common;
import static CinSamsungFront.common.DriverFactory.getDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import CinSamsungFront.common.DriverFactory;



public class BasePage {

	public static WebDriver driver;
	
	/******* Title *******/
	public String title() {
		return getDriver().getTitle();
	}
	
	/******* TextField *******/
    public void write(By by, String value) {
    	getDriver().findElement(by).clear();
    	getDriver().findElement(by).sendKeys(value);
    }

    
    public void clear(By by) {
    	getDriver().findElement(by).clear();
    }
    /******* Button *******/
    public void button(By by) {
    	getDriver().findElement(by).click();
    }
    
    public void option(By by) {
    	getDriver().findElement(by).click();
    }
    
    public String geButtontLabel(By by) {
    	return getDriver().findElement(by).getText();
    }
    
    /******* Message *******/
    public String message(By by) {
    	return getDriver().findElement(by).getText();
    }
    
    /******* Header *******/
    public String header(By by) {
      return getDriver().findElement(by).getText();
    }   
    
    
    /******* Combo *******/
    public void option(By by, String value) {
    	WebElement element=getDriver().findElement(by);
    	Select option=new Select(element);
    	option.selectByVisibleText(value);
    }
    
    /******* Text *******/
    public String text(By by) {
    	return getDriver().findElement(by).getText();
    }
    
    public String value(By by) {
    	return getDriver().findElement(by).getText();
    }
    
}
