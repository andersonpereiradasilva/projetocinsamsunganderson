package CinSamsungFront.common;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {
	
	public static WebDriver driver;
	
	public static WebDriver getDriver() {
		if (driver==null) {
			switch (Properties.broswer) {
			case FIREFOX: driver=new FirefoxDriver();break;
			case CHROME: driver=new ChromeDriver();break;
			}
			driver.manage().window().maximize();
		}
		return driver;
	}
	
	public static void killDriver() {
		if (driver!=null) {
			driver.quit();
			driver=null;
		}
	}
	
}
