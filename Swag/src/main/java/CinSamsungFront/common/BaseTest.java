package CinSamsungFront.common;
import static CinSamsungFront.common.DriverFactory.getDriver;
import static CinSamsungFront.common.DriverFactory.killDriver;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Rule;
import org.junit.rules.TestName;
//import org.junit.rules.Timeout;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


public class BaseTest {
	
	@Rule
	public TestName testName = new TestName();
	 //public Timeout globalTimeout = Timeout.millis(500);

	
	@After
	public void ends() throws IOException {
		
		TakesScreenshot ss = (TakesScreenshot) getDriver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File("target" + File.separator + "screenshot" +
				File.separator + testName.getMethodName() + ".jpg"));
		if (Properties.CLOSE_BROSWER)
			killDriver();
		
		
	}

}
