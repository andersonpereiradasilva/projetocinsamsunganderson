package CinSamsungFront.tests;
import static CinSamsungFront.common.DriverFactory.getDriver;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import CinSamsungFront.common.BaseTest;
import CinSamsungFront.common.DriverFactory;
import CinSamsungFront.pages.LoginPage;
import CinSamsungFront.pages.MenuPage;
import CinSamsungFront.pages.ProductsPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestMenuPage extends BaseTest {

    ProductsPage pageProd=new ProductsPage();
    MenuPage pageMenu=new MenuPage();
    LoginPage pageLogin= new LoginPage();
    
    
    @Test
	public void teste1_OpenMenu() {
		pageMenu.openMenu();
		Assert.assertEquals("All Items\n" + 
				"About\n" + 
				"Logout\n" + 
				"Reset App State",pageMenu.getMenu());
		pageMenu.closeMenu();
	}
	@Test
	public void teste2_MenuResetApp() {
		String id="1";
		pageProd.AddToCart(id);
		pageMenu.openMenu();
		pageMenu.MenuResetAppState();
		pageMenu.closeMenu();
		Assert.assertEquals("ADD TO CART",pageProd.getTextAddToCart(id));

	}

}
