package CinSamsungFront.tests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import CinSamsungFront.common.BaseTest;
import CinSamsungFront.pages.LoginPage;
import CinSamsungFront.pages.MenuPage;
import CinSamsungFront.pages.ProductsPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestProducts extends BaseTest {

    ProductsPage pageProd=new ProductsPage();
    MenuPage pageMenu=new MenuPage();
    LoginPage pageLogin= new LoginPage();
    
       
    @Test
	public void teste1_sortByDescendingName() {
		pageProd.sortDescendingName();
		Assert.assertEquals("Test.allTheThings() T-Shirt (Red)",pageProd.getProduct());
	}
    @Test
	public void teste2_SortByAscendingName() {

		pageProd.sortAscendingName();
		Assert.assertEquals("Sauce Labs Backpack",pageProd.getProduct());
	}
	@Test
	public void teste3_PriceLowToHigh() {
		pageProd.PriceLowToHigh();
		Assert.assertEquals("Sauce Labs Onesie",pageProd.getProduct());
	}
	
	@Test
	public void teste4_PriceHighToLow() {
		pageProd.PriceHighToLow();
		Assert.assertEquals("Sauce Labs Fleece Jacket",pageProd.getProduct());
	}	
	
	
	@Test
	public void teste5_addCart() {
		pageProd.addProduct("1");
		Assert.assertEquals("REMOVE", pageProd.getLabelButton("1"));
		Assert.assertEquals("1", pageProd.getCartItems());
		
	}
	
}
