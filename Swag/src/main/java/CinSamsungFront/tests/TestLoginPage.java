package CinSamsungFront.tests;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import CinSamsungFront.common.BaseTest;
import CinSamsungFront.pages.LoginPage;
import CinSamsungFront.pages.MenuPage;
import CinSamsungFront.pages.ProductsPage;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestLoginPage extends BaseTest {
	
	LoginPage pageLogin= new LoginPage();
	ProductsPage pageProd=new ProductsPage();
	MenuPage pageMenu = new MenuPage();
	

	@Test
	public void teste1_loginRequiredElements() {
		pageLogin.login();
		Assert.assertEquals("Epic sadface: Username is required", pageLogin.errorMessage());
   }

	@Test
	public void teste2_loginPasswordRequired() {
		pageLogin.setUserName("standard_user");
		pageLogin.login();
		Assert.assertEquals("Epic sadface: Password is required", pageLogin.errorMessage());
   }

	@Test
	public void teste3_LoginUsernameRequired() throws InterruptedException {
		pageLogin.setPassword("secret_sauce");
		pageLogin.setUserName("");
		pageLogin.login();
		Assert.assertEquals("Epic sadface: Username is required", pageLogin.errorMessage());
   }
	
	@Test
	public void teste4_loginBlocked() {
		pageLogin.setUserName("locked_out_user");
		pageLogin.setPassword("secret_sauce");
		pageLogin.login();
		
		Assert.assertEquals("Epic sadface: Sorry, this user has been locked out.", pageLogin.errorMessage());
	}
	
	@Test
	public void teste5_loginProblemUser() {
		pageLogin.setUserName("problem_user");
		pageLogin.setPassword("secret_sauce");
		pageLogin.login();
		pageMenu.openMenu();
		pageMenu.logout();
		Assert.assertFalse("//a[contains(@id,'item_')]//img[contains(@src,'jpgWithGarbageOnItToBreakTheUrl')].", true);
	}
	
	@Test
	public void teste6_loginSuccess() {
		
		pageLogin.setUserName("standard_user");
		pageLogin.setPassword("secret_sauce");
		pageLogin.login();
		Assert.assertEquals("Products", pageProd.getHeaderTitle());
		//pageMenu.openMenu();
		//pageMenu.logout();

	}

}
